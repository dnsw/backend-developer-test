# Drupal 8 Developer Test for Destination NSW

## Purpose
The purpose of this test is to determine your proficiency with Drupal 8 and integrating Web Services.

## Task
Build a Drupal 8 single page application that uses data provided from the [ATDW Atlas API](https://developer.atdw.com.au/ATDWO-api.html) to list accommodation options in New South Wales.

The visual result we want is to have a single page application with a single form, which should use the Drupal 8 Form API. When the form is submitted, it should return products that have been retreived on the fly from the remote API. ***The ATDW uses UTF-16LE encoding.***

The user should have the option to filter products by the following regions, as well as displaying all products:

- Blue Mountains
- Central Coast
- Country NSW
- Hunter
- Lord Howe Island
- North Coast
- Outback NSW
- Snowy Mountains
- South Coast

You will need to figure out how to do this in the ATDW documentation. Please also ensure your responses from the ATLAS API are returned in JSON.

### Data source
Data can be called using the methods defined at [ATDW Atlas API](https://developer.atdw.com.au/ATDWO-api.html). All documentation can be found via this link.
For development the API key is: `2015201520159`

### What we are looking for
* Use of Drupal 8 functionality
* Coding style (readable and concise)
* Use of third party libraries where appropriate
* Documentation

### Bonus
Additional consideration will be given for:

* Publicly hosting your solution
* Reporting a count of total products found
* Implementation of pagination
* Following of best practice Drupal coding standards using codesniffer.
* Test(s)

### Gotchas
The ATDW uses UTF-16LE encoding.